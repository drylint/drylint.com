
# 索引和约束

## 索引

在关系数据库中，如果有上万甚至上亿条记录，在查找记录的时候，想要获得非常快的速度，就需要使用索引。

索引是关系数据库中对某一列或多个列的值进行预排序的数据结构。通过使用索引，可以让数据库系统不必扫描整个表，而是直接定位到符合条件的记录，这样就大大加快了查询速度。

索引有：

- 普通索引，允许在索引列中插入重复值和空值
- 唯一索引，不允许重复值和空值
- 单列索引，一个索引只包含一个列
- 组合索引，多个字段组合创建的索引
- 全文索引，对支持全文查找的列进行索引，只能在 `CHAR | VARCHAR | TEXT` 列上创建，MySQL 中只有 MyISAM 引擎支持全文索引
- 空间索引，对空间数据类型的列创建的索引，MySQL 中只有 MyISAM 引擎支持空间索引

在创建表时指定索引列：

```sql
CREATE TABLE <表名> (
  <列名> <数据类型> ...,
  ...
  [UNIQUE | FULLTEXT | SPATIAL] INDEX/KEY [<索引名>]
  (<列名> [<索引长度>]) [ASC | DESC]
);
```

- `UNIQUE | FULLTEXT | SPATIAL` 为可选参数，分别表示唯一索引，全文索引，空间索引，不指定时表示创建普通索引。
- `INDEX` 也可以写作 `KEY` ，作用完全一样
- 索引名也是可选的，不指定的话，默认为列名
- 列名必须指定，索引长度为可选，只有字符串类型的列名才能制定索引长度
- `ASC | DESC` 可选，指定使用升序索引或者降序索引

因为 MySQL 中只有 MyISAM 引擎支持全文索引和空间索引，而默认存储引擎为 InnoDB， 所以全文索引和空间索引搜需要在创建表时修改表的存储引擎为 MyISAM ，不然创建索引会出错。

在已经存在的表上创建索引：

```sql
# 查看表中的索引
SHOW INDEX FROM <表名>;

# 使用 ALTER TABLE 添加索引
ALTER TABLE <表名>
ADD [UNIQUE | FULLTEXT | SPATIAL] INDEX/KEY [<索引名>]
(<列名> [<索引长度>]) [ASC | DESC];

# 使用 CREATE INDEX 添加索引
CREATE [UNIQUE | FULLTEXT | SPATIAL] INDEX/KEY [<索引名>]
ON <表名> (<列名> [<索引长度>]) [ASC | DESC];
```

索引的效率取决于索引列的值是否散列，即该列的值如果越互不相同，那么索引效率越高。反过来，如果记录的列存在大量相同的值，例如 `gender` 列，大约一半的记录值是 `M` ，另一半是 `F` ，因此，对该列创建索引就没有意义。

可以对一张表创建多个索引。索引的优点是提高了查询效率，缺点是在插入、更新和删除记录时，需要同时修改索引，因此，索引越多，插入、更新和删除记录的速度就越慢。

对于主键，关系数据库会自动对其创建主键索引。使用主键索引的效率是最高的，因为主键会保证绝对唯一。

删除索引：

```sql
# 使用 ALTER TABLE 删除索引
ALTER TABLE <表名> DROP INDEX <索引名>;

# 使用 DROP INDEX 删除索引
DROP INDEX <索引名> ON <表名>;
```

## 约束 (constraint)

MySQL 可以对插入表中的数据进行特定的验证，只有满足条件的数据才允许进入数据库。

约束可以在建表（`CREATE TABLE`） 时添加，也可以在建表后使用 `ALTER TABLE` 添加、删除或修改。

约束的级别有 列级约束 和 表级约束 之分，列级约束只约束某一列的数据，表级约束可以一次约束多列的数据。

### 非空约束 `NOT NULL`

- 禁止不插入任何数据，必须给定具体的数据，否则会报错
- 如果插入的值是 `null`，不会报错，字符串变为空字符串，数值型保存的值变成 `0`。
- 非空约束只能定义列级，不能定义表级

```sql
CREATE TABLE <表名> (
  <列名1> <数据类型> NOT NULL,
  ...
);
```

### 唯一约束 `UNIQUE`

使用 `UNIQUE` 约束的列不能插入重复相同的值，但是允许 `NULL`。可以给多列指定 `UNIQUE` 约束。

创建 `UNIQUE` 约束时，如果不定义约束名称，默认和列名相同。

在设计关系数据表的时候，看上去唯一的列，例如身份证号、邮箱地址等，因为他们具有业务含义，因此不宜作为主键。

但是，这些列根据业务要求，又具有唯一性约束：即不能出现两条记录存储了同一个身份证号。这个时候，就可以给该列添加一个唯一索引。

语法：

```sql
# 建表时指定列级唯一约束
CREATE TABLE <表名> (
  <列名1> <数据类型> UNIQUE,
  ...
);

# 建表时指定表级唯一约束
CREATE TABLE <表名> (
  <列名1> <数据类型>,
  ...
  [CONSTRAINT <约束名>] UNIQUE (<列名>)
);
```

```sql
ALTER TABLE <tableName>
ADD UNIQUE INDEX <indexName> (<columnName>);
```

也可以只对某一列添加一个唯一约束而不创建唯一索引：

```sql
ALTER TABLE <表名>
ADD CONSTRAINT <外键名>
UNIQUE (<列名>);
```

### 自增 `AUTO_INCREMENT`

给某列设置自增后，每增加一行数据，该列的值就是上一列的值加 `1` ，这样就不需要手动分配一个编号，同时该列也不会出现重复的值。

`AUTO_INCREMENT` 的初始值默认为 `1` ，一张表只能有一个字段使用自增，且这个字段必须是主键或联合主键的成员。自增列的数据类型只能为整数类型 (`TINYINT | SMALLINT | INT | BIGINT`) 。

设置了自增的列，在插入数据时不需要手动指定值，但不代表不可以手动指定，自增的列依然可以手动指定一个值，但该值必须是在之前没有出现过在当前列上的值。后续插入的行如果没有手动指定这个自增列的值，那么就会以上一行指定的值加 `1` 作为新的自增值。

语法：

```sql
CREATE TABLE <表名> (
  <列名1> <整数数据类型> PRIMARY KEY AUTO_INCREMENT,
  ...
);
```

通过自增插入的值通常不知道具体是什么值，这导致一种情况，比如还需要在另一个表中插入数据，其中有一列需要这个自增的值，怎么获取这个自增的值呢？

可使用 `last_insert_id()` 函数获得这个值，`SELECT last_insert_id()` 语句返回最后一个 AUTO_INCREMENT 值，然后可以将它用于后续的MySQL语句。

使用 `SELECT last_insert_id()` 时要注意，当一次插入多条记录时，只是获得第一条插入的自增值。

### 主键约束 `PRIMARY KEY`

在关系数据库中，一张表中的每一行数据被称为一条记录，能够通过某个字段的唯一值区分出不同的记录，这个字段被称为 **主键（PRIMARY KEY）**。

`PRIMARY KEY` 相当于 `NOT NULL` + `UNIQUE` 的组合，即不允许为空，也不允许重复。

每个表最多只允许一个主键，系统默认会在使用了 `PRIMARY KEY` 的列上建立对应的唯一索引，查询主键约束的列会更快。

语法：

```sql
# 建表时指定列级主键约束
CREATE TABLE <表名> (
  <列名1> <数据类型> PRIMARY KEY,
  ...
);

# 建表时指定表级主键约束
CREATE TABLE <表名> (
  <列名1> <数据类型>,
  ...
  [CONSTRAINT <约束名>] PRIMARY KEY (<列名>)
);
```

示例：

```sql
# 建表时指定列级主键约束
CREATE TABLE user (
  user_id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
  user_name CHAR(50) NOT NULL,
  user_gender CHAR(1) NOT NULL,
  user_age INT UNSIGNED
);

# 建表时指定表级主键约束
CREATE TABLE user (
  user_id INT UNSIGNED AUTO_INCREMENT,
  user_name CHAR(50) NOT NULL,
  user_gender CHAR(1) NOT NULL,
  user_age INT UNSIGNED,
  PRIMARY KEY (user_id)
);
```

基本原则：不使用任何业务相关的字段作为主键，比如名称，电话，身份证号，邮箱地址等。

通常，一般都会将命名为 `id` 的字段当作主键，常见 `id` 类型：

1. 自增整数类型：数据库会在插入数据时自动为每一条记录分配一个自增整数，这样我们就完全不用担心主键重复，也不用自己预先生成主键；

2. 全局唯一 GUID 类型：使用一种全局唯一的字符串作为主键，类似`8f55d96b-8acc-4636-8cb8-76bf8abc2f57` 。GUID 算法通过网卡 MAC 地址、时间戳和随机数保证任意计算机在任意时间生成的字符串都是不同的，大部分编程语言都内置了 GUID 算法，可以自己预算出主键。

对于大部分应用来说，通常自增类型的主键就能满足需求。如果使用 `INT` 自增类型，那么当一张表的记录数超过 2147483647 （约21亿）时，会达到上限而出错。使用 `BIGINT` 自增类型则可以最多约 922 亿亿条记录。

关系数据库实际上还允许通过多个字段唯一标识记录，即两个或更多的字段都设置为主键，这种主键被称为联合主键，但通常不建议使用。

### 外键约束 `FOREIGN KEY`

在一张表中，通过某个字段，可以把数据与另一张表关联起来，这种列称为 **外键**。外键的值必须等于另一张表中的某一行的主键字段的值。

外键并不是通过列名实现的，而是通过定义外键约束实现的，声明了外键约束的列，插入的值必须在另一个表的主键列上出现过。

外部键列和引用键(reference key)列可以位于相同的表中(自引用完整性约束)。

另一张表（也就是主键所在的表）成为主表或父表，外键所在的表成为从表或字表。

语法：

```sql
# 建表时指定外键约束
CREATE TABLE <表名> (
  <列名1> <数据类型>,
  ...
  [CONSTRAINT <约束名>] FOREIGN KEY(<列名>)
  REFERENCES <主表表名> (<关联列名>)
);
```

示例：

```sql
CREATE TABLE user (
  user_id INT UNSIGNED AUTO_INCREMENT,
  user_name CHAR(50) NOT NULL,
  user_gender CHAR(1) NOT NULL,
  user_age INT UNSIGNED,
  user_role INT UNSIGNED,
  PRIMARY KEY (user_id),
  CONSTRAINT fk_user_role FOREIGN KEY(user_role)
  REFERENCES <role> (role_id)
);
```

```sql
ALTER TABLE <表名>
ADD CONSTRAINT <约束名>
FOREIGN KEY (<外键列名>)
REFERENCES <关联表名> (关联列名);
```

示例：

```sql
ALTER TABLE students
ADD CONSTRAINT fk_class_id
FOREIGN KEY (class_id)
REFERENCES classes (id);
```

其中，外键约束的名称 `fk_class_id` 可以自己任意命名，`FOREIGN KEY (class_id)` 指定了`class_id` 作为外键，`REFERENCES classes (id)` 指定了这个外键将关联到`classes` 表的 `id` 列。

通过定义外键约束，关系数据库可以保证无法插入无效的数据。即如果 `classes` 表不存在 `id=99` 的记录， `students` 表就无法插入 `class_id=99` 的记录。

由于外键约束会降低数据库的性能，大部分互联网应用程序为了追求速度，并不设置外键约束，而是仅靠应用程序自身来保证逻辑的正确性。

删除一个外键约束，也是通过 `ALTER TABLE` 实现的：

```sql
ALTER TABLE <表名>
DROP FOREIGN KEY <外键名>;
```

```sql
ALTER TABLE students
DROP FOREIGN KEY fk_class_id;
```

`FOREIGN KEY` 约束末尾还支持以下关键字：

- `ON DELETE CASCADE` 当删除所引用的父表记录时,删除子表中相关的记录
- `ON DELETE SET NULL` 与上面不同的是删除时,转换子表中相关记录为 `NULL` 值
- `ON UPDATE CASCADE` 当更新所引用的父表记录时,更新子表中相关的记录
- `ON UPDATE SET NULL` 与上面不同的是更新时,转换子表中相关记录为 `NULL` 值
- `RESTRICT` 拒绝父表删除和更新

默认情况下,如果没有指定以上两个其中一个，则父表中被引用的记录将不能被删除。

```sql
FOREIGN KEY (本表的列名) REFERENCES 另一张表(表中的列名)
```

#### 多对多

多对多关系实际上是通过两个一对多关系实现的，即通过一个中间表，关联两个一对多关系，就形成了多对多关系。

#### 一对一

一对一关系是指，一个表的记录对应到另一个表的唯一一个记录。

用于将一个大表拆分成两个小表，目的是把经常读取和不经常读取的字段分开，以获得更高的性能。

### 默认值约束 `DEFAULT`

使用 `DEFAULT` 关键字为列声明默认值，两种方法使用：

设置默认值，使用 `DEFAULT <值>`。

语法：

```sql
CREATE TABLE <表名> (
  <列名1> <数据类型> DEFAULT <默认值>,
  ...
);
```

```sql
CREATE TABLE users (
  id INT PRIMARY KEY AUTO_INCREMENT,
  user_name CHAR(50) NOT NULL,
  user_gender TINYINT DEFAULT 2,
);
```

使用默认值，在插入数据时，值写为 `DEFAULT` 即表示使用默认值。

```sql
# 插入 DEFAULT 表示这里取默认值
INSERT INTO users VALUES(NULL, 'Tom', DEFAULT);

 # 不写也表示取默认值
INSERT INTO users (user_name) VALUES('Tom');
```

与大多数 DBMS 不一样， MySQL 不允许使用函数作为默认值，它只支持常量。

许多数据库开发人员使用默认值而不是 NULL 列，特别是对用于计算或数据分组的列更是如此。

### CHECK 检查约束

检查约束对要插入的数据进行检验

```sql
CREATE TABLE user(
  score TINYINT CHECK(score >= 0 AND score <= 100)
);
```

MySQL 不支持检查约束 `CHECK`，它认为会降低数据的插入速度。

### 约束命令语句

#### 添加约束

```sql
ALTER TABLE <表名>
ADD CONSTRAINT <约束名>
FOREIGN KEY(<列名>)
REFERENCES <另一表名>(<表中列名>);
```

#### 删除约束

```sql
ALTER TABLE <表名> DROP CONSTRAINT <约束名>;
ALTER TABLE <表名> DROP FOREIGN KEY <约束名>;
ALTER TABLE <表名> DROP PRIMARY KEY CASCADE;
```

#### 删除 NOT NULL 约束,用 `ALTER TABLE MODIFY` 子句来删除

```sql
ALTER TABLE user MODIFY phone varchar(11) NULL;
```

#### 删除外键约束

```sql
ALTER TABLE user DROP INDEX email;
ALTER TABLE user DROP FOREIGN KEY constraint_name;
```

#### 关闭约束

```sql
# 如果没有被引用则不需 CASCADE 关键字
ALTER TABLE user DISABLE CONSTRAINT constraint_name CASCADE;
```

#### 打开约束

```sql
# 注意，打开一个先前关闭的被引用的主键约束,并不能自动打开相关的外键约束
ALTER TABLE user ENABLE CONSTRAINT constraint_name;
```

#### 从约束合集视图中查询约束的信息

```sql
SELECT constraint_name, constraint_type, search_condition
  FROM user_constraints WHERE table_name='users';
```
